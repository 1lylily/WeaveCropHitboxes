package me.lily.cropfix.mixin

import me.lily.cropfix.Mod
import net.minecraft.block.BlockCrops
import net.minecraft.util.AxisAlignedBB
import net.minecraft.util.BlockPos
import net.minecraft.util.MovingObjectPosition
import net.minecraft.util.Vec3
import net.minecraft.world.World
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable

@Mixin(BlockCrops::class)
abstract class MixinBlockCrops : MixinBlock() {

    override fun getSelectedBoundingBox(
        world: World?,
        pos: BlockPos?,
        cir: CallbackInfoReturnable<AxisAlignedBB?>?) {
        if (world != null && pos != null) {
            Mod.updateCrops(world, pos, world.getBlockState(pos).block)
        }
    }

    override fun collisionRayTrace(
        world: World?,
        pos: BlockPos?,
        start: Vec3?,
        end: Vec3?,
        cir: CallbackInfoReturnable<MovingObjectPosition?>?
    ) {
        if (world != null && pos != null) {
            Mod.updateCrops(world, pos, world.getBlockState(pos).block)
        }
    }

}