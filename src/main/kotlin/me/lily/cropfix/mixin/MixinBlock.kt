package me.lily.cropfix.mixin

import net.minecraft.block.Block
import net.minecraft.util.AxisAlignedBB
import net.minecraft.util.BlockPos
import net.minecraft.util.MovingObjectPosition
import net.minecraft.util.Vec3
import net.minecraft.world.World
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.injection.At
import org.spongepowered.asm.mixin.injection.Inject
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable

@Mixin(Block::class)
abstract class MixinBlock {
    @Inject(method = ["getSelectedBoundingBox"], at = [At("HEAD")])
    open fun getSelectedBoundingBox(world: World?, pos: BlockPos?, cir: CallbackInfoReturnable<AxisAlignedBB?>?) {
    }

    @Inject(method = ["collisionRayTrace"], at = [At("HEAD")])
    open fun collisionRayTrace(
        world: World?,
        pos: BlockPos?,
        start: Vec3?,
        end: Vec3?,
        cir: CallbackInfoReturnable<MovingObjectPosition?>?
    ) {
    }
}