rootProject.name = "WeaveCropHitboxes"

pluginManagement {
    repositories {
        gradlePluginPortal()
        maven("https://jitpack.io")
    }
}