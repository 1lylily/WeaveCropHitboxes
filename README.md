# WeaveCropHitboxes
Port of Patcher's 1.12 crop hitboxes. <br>
Meant for use on Hypixel, but not server locked like Patcher. <br>
All the credit goes to Patcher, I just ported it to Kotlin and Weave.