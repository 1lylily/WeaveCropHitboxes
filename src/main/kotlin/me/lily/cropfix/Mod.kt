package me.lily.cropfix

import me.lily.cropfix.mixin.BlockAccessor
import net.minecraft.block.Block
import net.minecraft.block.BlockCarrot
import net.minecraft.block.BlockCrops
import net.minecraft.block.BlockNetherWart
import net.minecraft.block.BlockPotato
import net.minecraft.util.AxisAlignedBB
import net.minecraft.util.BlockPos
import net.minecraft.world.World
import net.weavemc.loader.api.ModInitializer

class Mod : ModInitializer {
    // this is pointless, this mod is just a few mixins
    override fun preInit() {}

    companion object {
        private val carrotPotatoBoxes: Array<AxisAlignedBB> = arrayOf(
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.125, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.1875, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.25, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.3125, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.375, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.4375, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.5, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.5625, 1.0)
        )

        private val wheatBoxes: Array<AxisAlignedBB> = arrayOf(
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.125, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.25, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.375, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.5, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.625, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.75, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.875, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 1.0, 1.0)
        )

        private val wartBoxes: Array<AxisAlignedBB> = arrayOf(
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.3125, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.5, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.6875, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.875, 1.0)
        )

        fun updateCrops(world: World, pos: BlockPos, block: Block) {
            val blockState = world.getBlockState(pos)
            val ageValue = blockState.getValue(BlockCrops.AGE)
            val accessor = block as BlockAccessor

            accessor.setMaxY(
                if(blockState.block is BlockCarrot
                    || blockState.block is BlockPotato) {
                    carrotPotatoBoxes[ageValue].maxY
                } else {
                    wheatBoxes[ageValue].maxY
                }
            )
        }

        fun updateWart(world: World, pos: BlockPos, block: Block) {
            val blockState = world.getBlockState(pos)
            val ageValue = blockState.getValue(BlockNetherWart.AGE)
            val accessor = block as BlockAccessor

            accessor.setMaxY(wartBoxes[ageValue].maxY)
        }
    }
}